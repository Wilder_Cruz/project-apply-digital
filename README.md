# ApplyDigital Test Junior BackEnd Developer

## Requirements
- Node 18
- Git
- Yarn

Node, git, and yarn can be installed through [homebrew](https://brew.sh/) on MacOS. If you need to support more than one version of node at the same time, you can consider installing it though [nvm](https://github.com/nvm-sh/nvm) instead of homebrew

## Getting started

### Docker
You can run the code locally in Docker, which avoids needing to install yarn.

```sh
git clone git@gitlab.com:api_wjca/project-apply-digital.git
cd project-apply-digital
docker-compose build
```

`docker-compose up` runs local production builds of the server app at http://localhost:3000

`docker-compose down` stops the running container.

`docker-compose run --rm bash` runs an interactive shell on the Docker image.

## Helpful Guides

- [Yarn docs](https://yarnpkg.com/en/docs)

### Environment variables - app server

- `PORT`: sets app server port.
- `MONGO_INITDB_ROOT_USERNAME`: sets mongodb username.
- `MONGO_INITDB_ROOT_PASSWORD`: sets mongodb password.
- `MONGO_DB`: sets mongodb database name.
- `MONGO_PORT`: sets mongodb port.
- `MONGO_HOST`: sets mongodb hostname.
- `MONGO_CONNECTION`: sets mongodb connection type.

## Example .env

```sh
PORT=3000
MONGO_INITDB_ROOT_USERNAME=root
MONGO_INITDB_ROOT_PASSWORD=root
MONGO_DB=mydb
MONGO_PORT=27017
MONGO_HOST=localhost
MONGO_CONNECTION=mongodb
```

## License
MIT
